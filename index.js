import solidityParser from 'solidity-parser';
import { inspect } from 'util';
import fs from 'fs';
import mkdirp from 'mkdirp';
import basename from 'basename';
import path from 'path';

const pipe = (...pipeline) => (input) => pipeline.reduce((acc, fn) => fn(acc), input);

const lineNumberByIndex = (index, string) => {
    let line = 0;
    let match;
    let re = /(^)[\S\s]/gm;
    while(match = re.exec(string)) {
        if(match.index > index) {
            break;
        }
        line++;
    }
    return line;
}

const escapeRegexp = (str) => {
    return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
}

const findAllMatches = (str, re) => {
    let matches = [];
    let match = re.exec(str);

    while (match != null) {
        matches.push(match);
        match = re.exec(str);
    }

    return matches;
}

const replaceAll = (input, re, cb) => {
    let match = re.exec(input);

    while (match != null) {
        let line = lineNumberByIndex(match.index, input);
        let replacement = cb(match, line);
        input = input.substring(0, match.index) + replacement + input.substring(match.index + match[0].length);
        let startIndex = match.index + replacement.length;
        match = re.exec(input.substring(startIndex));
        if(match) {
            match.index += startIndex;
        }
    }
    return input;
}

const resolveTypeAliases = (state) => {
    state.operationLog.push('## resolving type aliases');
    let typeTable = {};

    state.program = replaceAll(state.program, /using +([A-Za-z0-9]+) += +([\.A-Za-z0-9\[\]]+);?/, match => {
        typeTable[match[1]] = match[2]
        return '';
    });

    let replaces;
    while(replaces === undefined || replaces > 0) {
        replaces = 0;
        for(let t in typeTable) {
            state.program = replaceAll(state.program, new RegExp('([\\(\\),\\[\\] ])(' + escapeRegexp(t) + ')([\\(\\),\\[\\] ])'),
                (match, line) => {
                    state.operationLog.push(`line ${line}: Type replaced: ${t} -> ${typeTable[t]}`);
                    replaces++;
                    return match[1] + typeTable[t] + match[3];
                });
        }
    };
    state.operationLog.push('## type aliases resolved');
    return state;
}

const resolveDefines = (state) => {
    state.operationLog.push('## replacing defines');
    let defines = {};

    state.program = replaceAll(state.program, /define +([_A-Za-z0-9]+) += +([\.A-Za-z0-9\[\]]+);?/, match => {
        defines[match[1]] = match[2]
        return '';
    });

    let replaces;
    for(let t in defines) {
        state.program = replaceAll(state.program, new RegExp(escapeRegexp(t)),
            (match, line) => {
                state.operationLog.push(`line ${line}: Define replaced: ${t} -> ${defines[t]}`);
                replaces++;
                return defines[t];
            });
    }
    state.operationLog.push('## type aliases resolved');
    return state;
}

const buildStructList = (node, structs) => {
    if(node === undefined) return;

    if(node.type === 'StructDeclaration') {
        structs[node.name] = [];
        for(let i = 0; i < node.body.length; i++) {
            let field = {};
            field.name = node.body[i].name;
            field.type = node.body[i].literal.literal;
            structs[node.name].push(field);
        }
        return;
    }
    if(!node.body) return;
    for(let i = 0; i < node.body.length; i++) {
        buildStructList(node.body[i], structs);
    }
}

const buildFuncList = (node, funcs) => {
    if(node === undefined) return;
    if(!node.body) return;

    if(!node.modifiers) {
        node.modifiers = [];
    }

    if(node.type === 'FunctionDeclaration') {
        funcs[node.name] = {
            start: node.body.start,
            end: node.body.end,
            internal: node.modifiers.find(mod => mod.name === 'internal') !== undefined,
        }
        return;
    }
    if(!node.body) return;
    for(let i = 0; i < node.body.length; i++) {
        buildFuncList(node.body[i], funcs);
    }
}

const buildEnumList = (node, enums) => {
    if(!node === undefined) return;

    if(node.type === 'EnumDeclaration') {
        enums[node.name] = {
            start: node.start,
            end: node.end,
            members: node.members,
        }
        return;
    }
    if(!node.body) return;
    for(let i = 0; i < node.body.length; i++) {
        buildEnumList(node.body[i], enums);
    }
}

const buildAnalysis = (state) => {
    state.operationLog.push('## beginning analysis');
    state.operationLog.push('## building AST');
    let ast;
    try {
        ast = solidityParser.parse(state.program);
    } catch(err) {
        state.operationLog.push(err);
        console.log(state.program);
        throw(err);
    }

    state.analysis = { structs: {}, funcs: {}, enums: {} };
    buildStructList(ast, state.analysis.structs);
    buildFuncList(ast, state.analysis.funcs);
    buildEnumList(ast, state.analysis.enums);

    const structMatches = findAllMatches(state.program, new RegExp('/\\* externalstruct: (.*) \\*/', 'g'));
    for(let match of structMatches) {
        const parsed = JSON.parse(match[1]);
        state.analysis.structs[parsed[0]] = parsed[1];
    }

    const enumMatches = findAllMatches(state.program, new RegExp('/\\* externalenum: (.*) \\*/', 'g'));
    for(let match of enumMatches) {
        const parsed = JSON.parse(match[1]);
        state.analysis.enums[parsed[0]] = parsed[1];
    }

    state.operationLog.push(`found ${Object.keys(state.analysis.structs).length} structs`);
    state.operationLog.push(`found ${Object.keys(state.analysis.funcs).length} functions`);
    state.operationLog.push('## analysis done');
    return state;
}

const expandParameterList = (state, functionName, inParams, paramType, additionBegin, additionEnd) => {
    let out = [];

    for(let i = 0; i < inParams.length; i++) {
        let [ initialType, name ] = inParams[i].split(' ');
        if(name === undefined) {
            throw new Error(`unnamed parameters not supported: function ${functionName}`);
        }
        let arrayQualifier = '';
        let arrayMatch = initialType.match(/(.*)(\[[0-9]*\])/);
        let type;
        if(arrayMatch) {
            type = arrayMatch[1];
            arrayQualifier = arrayMatch[2];
        } else {
            type = initialType;
        }
        if(state.analysis.enums[type]) {
            type = 'uint8';
        }

        if(state.analysis.structs[type]) {
            if(arrayQualifier) {
                state.replacements.push({
                    functionName,
                    from: escapeRegexp(`${name}.length`),
                    to: `${name}_dot_${state.analysis.structs[type][0].name}.length`,
                });
            }
            const expanded = state.analysis.structs[type].map(f => `${name}_dot_${f.name}`).join(', ');
            state.replacements.push({
                functionName,
                from: `${escapeRegexp('/*expand*/')}${name}${escapeRegexp('/*expand*/')}`,
                to: `${expanded}`,
            });
            // add `Signature memory signature;`
            additionBegin.push(`\n${type}${arrayQualifier || ''} memory ${name};\n`);

            for(let sf in state.analysis.structs[type]) {
                const fname = state.analysis.structs[type][sf].name;
                const ftype = state.analysis.structs[type][sf].type;
                out.push({name: name + '_dot_' + fname, type: state.analysis.structs[type][sf].type + arrayQualifier});
                if(!arrayQualifier) {
                    // add `signature.v = signature_dot_v`;
                    if(paramType === 'in') {
                        additionBegin.push(`${name}.${fname} = ${ftype}(${name}_dot_${fname});`);
                    } else {
                        additionEnd.push(`${name}_dot_${fname} = ${ftype}(${name}.${fname});`);
                    }
                } else {
                    const iterator = `__${fname}_iterator__`;
                    if(paramType === 'in') {
                        additionBegin.push(`for(uint ${iterator} = 0; ${iterator} < ${name}_dot_${fname}.length;${iterator}++)`)
                        additionBegin.push(`  ${name}[${iterator}].${fname} = ${ftype}(${name}_dot_${fname}[${iterator}]);`);
                    } else {
                        additionEnd.push(`for(uint ${iterator} = 0; ${iterator} < ${name}_dot_${fname}.length; ${iterator}++)`)
                        additionEnd.push(`  ${name}_dot_${fname}[${iterator}] = uint8(${name}[${iterator}].${fname});`);
                    }
                }
            }
        } else {
            out.push({type: type + arrayQualifier, name});
        }
    }
    return { state, out };
}

const expandComments = (state) => {
    state.operationLog.push('## expanding types in @param comments');
    state.program = replaceAll(state.program, new RegExp('\\* +@param +([A-Za-z0-9]+) +\\- +(.*)\\*\\*expandtype: (.+) \\*\\*'),
        (match, line) => {
            const [ , variable, desc, initialType ] = match;
            let arrayQualifier = '';
            let arrayMatch = initialType.match(/(.*)(\[[0-9]*\])/);
            let type;
            if(arrayMatch) {
                type = arrayMatch[1];
                arrayQualifier = arrayMatch[2];
            } else {
                type = initialType;
            }
            let result = [];
            for(let sf of state.analysis.structs[type]) {
                result.push(`* @param ${variable}_dot_${sf.name} - ${desc} (${type}.${sf.name} fields)`);
            }
            return result.join('\n');
        });
    state.operationLog.push('## done expanding types in @param comments')
    return state;
}

const forceExpand = (state) => {
    state.operationLog.push('## force expanding variables');
    state.program = replaceAll(state.program, new RegExp('/\\*declareforceexpand\\*/ *(.*?)(\\[\\d+\\]) (.*?) */\\*declareforceexpand\\*/;'),
        (match, line) => {
            const [ , type, arrayQualifier, variable ] = match;
            if(arrayQualifier) {
                let result = '';
                for(let sf of state.analysis.structs[type]) {
                    result += `\nuint8${arrayQualifier} memory ${variable}_dot_${sf.name};`;
                }
                // result += `\nfor(i = 0; i < ${arrayQualifier.match(/\[(\d+)\]/)[1]}; i++) {`
                // for(let sf of state.analysis.structs[type]) {
                //     result += `\n${variable}_dot_${sf.name}[i] = uint8(${variable}[i].${sf.name});`;
                // }
                // result += `}`;
                return result;
            } else {
                throw new Error('not implemented');
            }
        });

    state.program = replaceAll(state.program, new RegExp('/\\*forceexpand\\*/ *(.*?)(\\[\\d+\\]) (.*?) */\\*forceexpand\\*/'),
        (match, line) => {
            const [ , type, arrayQualifier, variable ] = match;
            if(arrayQualifier) {
                let result = state.analysis.structs[type].map(
                    f => `${variable}_dot_${f.name}`).join(', ');
                return result;
            } else {
                throw new Error('not implemented');
            }
        });

    state.operationLog.push('## done force expanding variables');
    return state;
}

const expandFunctions = (state) => {
    state.operationLog.push('## expanding function parameters');

    let dirty = true;
    while(dirty) {
        dirty = false;
        state.program = replaceAll(state.program, /function +([A-Za-z0-9_]+) *\((.*?)\)(.*?)(?:returns *\((.*?)\))? *\{/,
            (match, line) => {
                let [ wholeMatch, functionName, inParamsString, beforeReturns, inReturnsParamsString ] = match;

                state.operationLog.push(`function ${functionName}:`);
                if(state.analysis.funcs[functionName].internal) {
                    state.operationLog.push(`internal, don't expand parameters`);
                    return wholeMatch;
                }

                // let inParams, inReturnParams;
                let outParams = [];
                let outReturnParams = [];

                let additionBegin = [];
                let additionEnd = [];

                if(inParamsString) {
                    let before = state.replacements.length;
                    ({ state, out: outParams } = expandParameterList(state, functionName, inParamsString.split(',').map(param => param.trim()), 'in', additionBegin, additionEnd));
                    if(state.replacements.length !== before) {
                        dirty = true;
                    }
                }
                if(inReturnsParamsString) {
                    let before = state.replacements.length;
                    ({ state, out: outReturnParams } = expandParameterList(state, functionName, inReturnsParamsString.split(',').map(param => param.trim()), 'out', additionBegin, additionEnd));
                    if(state.replacements.length !== before) {
                        dirty = true;
                    }
                }

                // addition += outParams.map((param) => `${param.name} = ${param.name}; // silence warning (in parameter)`).join('\n') + '\n';
                // state.operationLog.push(inspect(inReturnParams, false, null));
                // addition += outReturnParams.map((param) => `${param.name} = ${param.name}; // silence warning (out parameter)`).join('\n') + '\n';

                let outParamsString = outParams.map(param => param.type + ' ' + param.name).join(', ');
                let outReturnParamsString = '';
                if(outReturnParams.length > 0) {
                    outReturnParamsString = `returns (${outReturnParams.map(param => param.type + ' ' + param.name).join(', ')})`;
                }
                state.replacements.push({
                    functionName,
                    from: /(.*)\}$/,
                    to: `$1${additionEnd.join('\n')}}`,
                });
                return `function ${functionName} (${outParamsString}) ${beforeReturns} ${outReturnParamsString} { ${additionBegin.join('\n')}`;
            });
    }
    state.operationLog.push('## done expanding function parameters');
    return state;
}

const performReplacements = (state) => {
    state.operationLog.push(`## performing ${state.replacements.length} replacements`);

    let leftover = state.program;
    let funcs = [];
    let offset = 0;

    for(let f in state.analysis.funcs) {
        funcs[f] = {};

        state.operationLog.push(`operating on ${f}`);
        let start = state.analysis.funcs[f].start;
        let end = state.analysis.funcs[f].end;

        let prefix = leftover.substring(offset, offset + start);
        let body = leftover.substring(offset + start, offset + end);
        let suffix = leftover.substring(offset + end);

        offset -= (leftover.length - suffix.length);
        leftover = suffix;

        for(let r in state.replacements) {
            const repl = state.replacements[r];
            if(repl.functionName !== f) continue;
            state.operationLog.push(`replacing ${repl.from} to ${repl.to}`);
            body = body.replace(new RegExp(repl.from, 'g'), repl.to);
        }

        funcs[f].body = body;
        funcs[f].prefix = prefix;
    }

    state.program = '';

    for(let f in state.analysis.funcs) {
        state.program += funcs[f].prefix + funcs[f].body;
    }

    state.program += leftover;

    state.operationLog.push(`## done replacing`);
    return state;
}

const printProgram = (state) => {
    console.log(state.program);
    return state;
}

const saveEnums = (state) => {
    return state;
}

function isol2sol(program) {
    const result = pipe(resolveDefines, resolveTypeAliases, buildAnalysis, forceExpand, expandComments, expandFunctions, buildAnalysis, performReplacements, saveEnums)({
        program,
        operationLog: [],
        replacements: [] });

    return result;
}

export default isol2sol;

export function convertFile(file, outputDir) {
    const inputisol = fs.readFileSync(file, 'utf8');
    let program, operationLog, analysis;
    try {
        ({ program, operationLog, analysis } = isol2sol(inputisol));
        const baseName = basename(file);
        mkdirp.sync(outputDir);
        const outFile = path.join(outputDir, baseName + '.sol');
        const metaOutFile = `${outFile}.meta`;
        fs.writeFileSync(outFile, program, 'utf8');
        fs.writeFileSync(metaOutFile, JSON.stringify({ enums: analysis.enums }));
        return { outFile, metaOutFile }
    } catch(err) {
        console.log(operationLog);
        console.log(err);
        console.log(inputisol);
        throw(err);
    }
}
