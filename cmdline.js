import fs from 'fs';
import { convertFile } from './index';
import glob from 'glob';

let opt = require('node-getopt').create([
    ['f', 'files=ARG', 'glob expression of files to process'],
    ['o', 'output=ARG', 'output directory']])
.bindHelp()
.parseSystem();

const filenames = glob.sync(opt.options.files);
let contracts = [];
for(let file of filenames) {
    const { outFile, metaOutFile } = convertFile(file, opt.options.output);
    console.log(`${file} \u2192 ${outFile}, ${metaOutFile}`);
}
