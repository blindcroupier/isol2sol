import { inspect } from 'util';

const capitalizeTxt = (txt) => txt.charAt(0).toUpperCase() + txt.slice(1);

let enumReplacer;
let enumBackReplacer;

function buildInputFunctorsForArgs(args) {
    let realParamIdx = 0;
    let inStruct = false;
    let result = [];
    for(let i of args) {
        let name = i.name;
        let type = i.type;
        let arrayMatch = type.match(/(.*)(\[\d+\])/);
        let arrayMod;
        if(arrayMatch) {
            [ , type, arrayMod ] = arrayMatch;
        }
        let match = name.match(/(.*)_dot_(.*)/);
        if(!match) {
            if(inStruct) { realParamIdx++; }
            inStruct = false;
            (realParamIdx => result.push(params => params[realParamIdx]))(realParamIdx);
            realParamIdx++;
            continue;
        };
        let [ _, struct, field ] = match;
        if(!arrayMod) {
            (realParamIdx => result.push(params => enumReplacer(field, params[realParamIdx][field])))(realParamIdx);
        } else {
            (realParamIdx => result.push(params => params[realParamIdx].map(elem => enumReplacer(field, elem[field]))))(realParamIdx);
        }
        inStruct = true;
    }
    return result;
}

function buildOutputFunctorsForArgs(args) {
    function pushStructFunctor() {
        ((paramIdx, currentStruct) =>
            result.push(params => {
                let result = {};
                if(currentStruct.maxArrayIdx) {
                    result = [];
                    for(var i = 0; i < currentStruct.maxArrayIdx; i++) {
                        for(let fname in currentStruct[i]) {
                            result[i] = result[i] || {};
                            result[i][fname] = enumBackReplacer(fname, currentStruct[i][fname](params));
                        }
                    }
                } else {
                    for(let fname in currentStruct) {
                        if(fname === 'maxArrayIdx') {
                            continue;
                        }
                        result[fname] = currentStruct[fname](params);
                    }
                }
                return result;
            }))(paramIdx, currentStruct);
        currentStruct = {};
    }

    let inStruct;
    let result = [];
    let currentStruct = {};
    let paramIdx = 0;
    for(let i of args) {
        const match = i.name.match(/(.*)_dot_(.*)/);
        const arrayMatch = i.type.match(/.\[(\d+)\]/);
        let maxArrayIdx;
        if(arrayMatch) { maxArrayIdx = arrayMatch[1]; }
        if(!match) {
            if(inStruct) { pushStructFunctor(); }
            inStruct = false;
            if(i.type.match(/\[\d+\]/) && args.length === 1) {
                (paramIdx => result.push(params => params))(paramIdx);
            } else {
                (paramIdx => result.push(params => params[paramIdx]))(paramIdx);
            }
            paramIdx++;
            continue;
        };
        let [ _, struct, field ] = match;
        if(inStruct && inStruct !== struct) { pushStructFunctor(); }

        currentStruct.maxArrayIdx = maxArrayIdx;

        if(!currentStruct.maxArrayIdx) {
            (paramIdx => { currentStruct[field] = params => params[paramIdx]; })(paramIdx);
        } else {
            for(let i = 0; i < currentStruct.maxArrayIdx; i++) {
                ((paramIdx, i) => {
                    currentStruct[i] = currentStruct[i] || {};
                    currentStruct[i][field] = params => params[paramIdx][i];
                })(paramIdx, i);
            }
        }
        inStruct = struct;
        paramIdx++;
    }
    if(inStruct) {
        pushStructFunctor();
    }
    return result;
}

export const prepareABI = abi => {
    for(let f of abi) {
        if(f.type !== 'function') {
            continue;
        }
        for(let i of f.inputs) {
            let match = i.type.match(/(.*?)\.([A-Za-z]+)(\[\d+\])?/);
            if(match) {
                i.type = 'uint8';
                if(match[3]) {
                    i.type += match[3];
                }
            }
        }
    }
    return abi;
}

export default function prepareInstance(abi, functions, meta) {
    for(let f of abi) {
        if(f.type !== 'function') {
            continue;
        }
        let inFunctors = buildInputFunctorsForArgs(f.inputs);
        let outFunctors = buildOutputFunctorsForArgs(f.outputs);

        enumReplacer = (field, value) => {
            const enumName = capitalizeTxt(field);
            let idx = -1;
            for(let currentEnum in meta.enums) {
                let split = currentEnum.split('.');
                if(split[split.length - 1] !== enumName) continue;
                idx = meta.enums[currentEnum].members.indexOf(value);
            }
            if(idx !== -1) {
                return idx;
            }
            return value;
        }

        enumBackReplacer = (field, value) => {
            const enumName = capitalizeTxt(field);
            for(let currentEnum in meta.enums) {
                let split = currentEnum.split('.');
                if(split[split.length - 1] !== enumName) continue;
                let numberValue;
                if(value.toNumber) {
                    numberValue = value.toNumber();
                } else {
                    numberValue = value;
                }

                return meta.enums[currentEnum].members[numberValue];
            }
            return value;
        }

        functions[f.name] = (oldF => {
            return (...args) => {
                const initialArgs = args;
                args = inFunctors.map(f => f(args));
                const extraArgs = initialArgs.length - args.length;
                args.push(...initialArgs.slice(initialArgs.length - extraArgs, initialArgs.length));
                if(f.constant) {
                    return oldF(...args)
                        .then(oldReturns => {
                            if(!Array.isArray(oldReturns)) {
                                oldReturns = [oldReturns];
                            }

                            let newReturns = outFunctors.map(f => f(oldReturns));

                            if(newReturns.length === 1) {
                                newReturns = newReturns[0];
                            }
                            return newReturns;
                        })
                } else {
                    return oldF(...args);
                }
            }
        })(functions[f.name]);
    };
    return functions;
}
